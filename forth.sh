#!/bin/sh

# a forth interpreter in POSIX sh that doesn't use arithmetic expressions anywhere
# why? because i fucking can that's why

# number of bits of data words
DATA_MASK="$(seq -s ' ' ${DATA_BITS:-32})"
# number of bits of stack pointers (you shouldn't have to change that)
STACKPTR_MASK="$(seq -s ' ' ${STACKPTR_BITS:-10})"

__='_ _ _ _ _ _ _ _'
OP_AND="0 0 $__ 0 1"
OP_NAND="1 1 $__ 1 0"
OP_OR="0 1 $__ 1 1"
OP_NOR="1 0 $__ 0 0"
OP_XOR="0 1 $__ 1 0"
OP_XNOR="1 0 $__ 0 1"

# op2 sel r00 r01 _ _ _ _ _ _ _ _ r10 r11
op2() {
    shift $1
    echo -n $2
}

# bitwise2 mask operation result operand1 operand2
bitwise2() {
    for i in $1
    do
        eval "_a=\${$4_$i:-0}"
        eval "_b=\${$5_$i:-0}"
        eval "$3_$i=$(op2 $_a$_b $2)"
    done
}

# op_not sel 1 0
op_not() {
    shift $1
    echo -n $2
}

# copy mask result input
copy() {
    for i in $1
    do
        eval "$2_$i=\${$3_$i:-0}"
    done
}

# bitnot mask result input
bitnot() {
    for i in $1
    do
        eval "_a=\${$3_$i:-0}"
        eval "$2_$i=$(op_not $_a 1 0)"
    done
}

# add mask carry result operand1 operand2
add() {
    eval "_c=\${$2:-0}"
    for i in $1
    do
        eval "_a=\${$4_$i:-0}"
        eval "_b=\${$5_$i:-0}"
        _axb=$(op2 $_a$_b $OP_XOR)
        eval "$3_$i=$(op2 $_axb$_c $OP_XOR)"
        _aab=$(op2 $_a$_b $OP_AND)
        _axbac=$(op2 $_axb$_c $OP_AND)
        _c=$(op2 $_aab$_axbac $OP_OR)
    done
    eval "$2=$_c"
}

# sub mask borrow result operand1 operand2
sub() {
    bitnot "$1" _subb $5
    add "$1" $2 $3 $4 _subb
}

# cmp mask result operand1 operand2 -> true for lt, false for ge
cmp() {
    _xc=1
    sub "$1 cmp" _xc $2 $3 $4
    [ $_xc -eq 1 ]
}

# get_first_arg a ... -> a
get_first_arg() {
    echo -n $1
}

# reverse mask result input
reverse() {
    s=""
    for i in $1
    do
        eval "_c=\${$3_$i:-0}"
        s="$_c $s"
    done
    for i in $1
    do
        eval "$2_$i=$(get_first_arg $s)"
        s="${s#??}"
    done
}

# shleft mask result input
shleft() {
    _c=0
    for i in $1
    do
        eval "_c_buf=\${$3_$i:-0}"
        eval "$2_$i=$_c"
        _c=$_c_buf
    done
}

# shright mask result input
shright() {
    reverse "$1" _tmp $3
    shleft "$1" _tmp_new _tmp
    reverse "$1" $2 _tmp_new
}

# eq mask a b -> r
eq() {
    _r=1
    for i in $1
    do
        eval "_a=\${$2_$i:-0}"
        eval "_b=\${$3_$i:-0}"
        _r=$(op2 $_r$(op2 $_a$_b $OP_XNOR) $OP_AND)
    done
    [ $_r -eq 1 ]
}

# savebin mask result number...
savebin() {
    _m=$1
    _r=$2
    shift 2
    for i in $_m
    do
        eval "${_r}_$i=$1"
        shift 1
    done
}

# printbin mask operand -> number
printbin() {
    for i in $1
    do
        eval "echo -n \${$2_$i:-0}"
    done
}

# print_num var
print_num() {
    reverse "$DATA_MASK" _tmp $1
    printbin "$DATA_MASK" _tmp
    echo -n ' '
}

# clear mask var
clear() {
    for i in $1
    do
        eval "$2_$i=0"
    done
}

# _push sp stack var
_push() {
    copy "$DATA_MASK" $2_$(printbin "$STACKPTR_MASK" $1) $3
    _xc=0
    add "$STACKPTR_MASK" _xc $1 $1 const_1
}

# _pop sp stack var
_pop() {
    if eq "$STACKPTR_MASK" $1 const_0
    then
        clear "$DATA_MASK" $3
    else
        _xc=1
        sub "$STACKPTR_MASK" _xc $1 $1 const_1
        copy "$DATA_MASK" $3 $2_$(printbin "$STACKPTR_MASK" $1)
    fi
}

# push var
push() {
    _push stackptr stack $1
}
# pop var
pop() {
    _pop stackptr stack $1
}

# _push_string sp stack str
_push_string() {
    _sp=$(printbin "$STACKPTR_MASK" $1)
    eval "$2_$_sp=\"\$3\""
    _xc=0
    add "$STACKPTR_MASK" _xc $1 $1 const_1
}

# _pop_string sp stack
_pop_string() {
    _xc=1
    sub "$STACKPTR_MASK" _xc $1 $1 const_1
    _sp=$(printbin "$STACKPTR_MASK" $1)
    eval "_str=\"\$$2_$_sp\""
    echo -n "$_str"
}

push_repl_input() {
    _push_string repl_input_sp repl_input_stack "$_repl_input"
}
pop_repl_input() {
    _repl_input="$(_pop_string repl_input_sp repl_input_stack)"
}

# get_first_char word
get_first_char() {
    _s="$1"
    while [ ${#_s} -gt 1 ]
    do
        _s="${_s%?}"
    done
    echo -n "$_s"
}

MASK4="1 2 3 4"
savebin "$MASK4" dec_0 0 0 0 0
savebin "$MASK4" dec_1 1 0 0 0
savebin "$MASK4" dec_2 0 1 0 0
savebin "$MASK4" dec_3 1 1 0 0
savebin "$MASK4" dec_4 0 0 1 0
savebin "$MASK4" dec_5 1 0 1 0
savebin "$MASK4" dec_6 0 1 1 0
savebin "$MASK4" dec_7 1 1 1 0
savebin "$MASK4" dec_8 0 0 0 1
savebin "$MASK4" dec_9 1 0 0 1

# eval_operation oper
eval_operation() {
    case "$1" in
        '+')
            pop _op_b
            pop _op_a
            _xc=0
            add "$DATA_MASK" _xc _n _op_a _op_b
            push _n
            ;;
        '-')
            pop _op_b
            pop _op_a
            _xc=1
            sub "$DATA_MASK" _xc _n _op_a _op_b
            push _n
            ;;
        and)
            pop _op_b
            pop _op_a
            bitwise2 "$DATA_MASK" "$OP_AND" _n _op_a _op_b
            push _n
            ;;
        or)
            pop _op_b
            pop _op_a
            bitwise2 "$DATA_MASK" "$OP_OR" _n _op_a _op_b
            push _n
            ;;
        xor)
            pop _op_b
            pop _op_a
            bitwise2 "$DATA_MASK" "$OP_XOR" _n _op_a _op_b
            push _n
            ;;
        invert)
            pop _op
            bitnot "$DATA_MASK" _n _op
            push _n
            ;;
        lshift)
            pop _op_n
            pop _op
            until eq "$DATA_MASK" _op_n const_0
            do
                shleft "$DATA_MASK" _op _op
                _xc=1
                sub "$DATA_MASK" _xc _op_n _op_n const_1
            done
            push _op
            ;;
        rshift)
            pop _op_n
            pop _op
            until eq "$DATA_MASK" _op_n const_0
            do
                shright "$DATA_MASK" _op _op
                _xc=1
                sub "$DATA_MASK" _xc _op_n _op_n const_1
            done
            push _op
            ;;
        '2*')
            pop _op
            shleft "$DATA_MASK" _n _op
            push _n
            ;;
        '2/')
            pop _op
            shright "$DATA_MASK" _n _op
            push _n
            ;;
        '=')
            pop _op_b
            pop _op_a
            eq "$DATA_MASK" _op_a _op_b && push const_n1 || push const_0
            ;;
        '<>')
            pop _op_b
            pop _op_a
            eq "$DATA_MASK" _op_a _op_b && push const_0 || push const_n1
            ;;
        '>=')
            pop _op_b
            pop _op_a
            cmp "$DATA_MASK" _null _op_a _op_b && push const_n1 || push const_0
            ;;
        '<')
            pop _op_b
            pop _op_a
            cmp "$DATA_MASK" _null _op_a _op_b && push const_0 || push const_n1
            ;;
        '<=')
            pop _op_a
            pop _op_b
            cmp "$DATA_MASK" _null _op_a _op_b && push const_n1 || push const_0
            ;;
        '>')
            pop _op_a
            pop _op_b
            cmp "$DATA_MASK" _null _op_a _op_b && push const_0 || push const_n1
            ;;
        '0='|'0<>'|'0>='|'0<'|'0<='|'0>')
            push const_0
            eval_operation ${1#0}
            ;;
        '.')
            pop _a
            print_num _a
            ;;
        '.s')
            clear "$STACKPTR_MASK" _sp_iter
            until cmp "$STACKPTR_MASK" _null _sp_iter stackptr
            do
                _xc=0
                print_num stack_$(printbin "$STACKPTR_MASK" _sp_iter)
                add "$STACKPTR_MASK" _xc _sp_iter _sp_iter const_1
            done
            ;;
        drop)
            pop _null
            ;;
        dup)
            pop _a
            push _a
            push _a
            ;;
        swap)
            pop _a
            pop _b
            push _a
            push _b
            ;;
        over)
            pop _b
            pop _a
            push _a
            push _b
            push _a
            ;;
        rot)
            pop _c
            pop _b
            pop _a
            push _b
            push _c
            push _a
            ;;
        '-rot')
            pop _c
            pop _b
            pop _a
            push _c
            push _a
            push _b
            ;;
        nip)
            pop _b
            pop _a
            push _b
            ;;
        tuck)
            pop _b
            pop _a
            push _b
            push _a
            push _b
            ;;
        pick)
            pop _n
            copy "$STACKPTR_MASK" _stackptr_save stackptr
            _xc=1
            sub "$STACKPTR_MASK" _xc stackptr stackptr _n
            pop _a
            copy "$STACKPTR_MASK" stackptr _stackptr_save
            push _a
            ;;
        true)
            push const_n1
            ;;
        false)
            push const_0
            ;;
        bye)
            exit
            ;;
        ':')
            repl_string_mode=word
            repl_string=''
            ;;
        '."')
            repl_string_mode=print
            repl_string=''
            ;;
        '!"')
            repl_string_mode=exec
            repl_string=''
            ;;
        'if')
            repl_string_mode='if'
            repl_string=''
            clear "$STACKPTR_MASK" if_level
            ;;
        variable)
            repl_string_mode=variable
            repl_string=''
            ;;
        __v)
            repl_string_mode=variable_push
            repl_string=''
            ;;
        '!')
            pop _a
            _v="$(_pop_string var_sp var_stack)"
            eval "var_value_$_v=\$_a"
            ;;
        '@')
            _v="$(_pop_string var_sp var_stack)"
            eval "_a=\${var_value_$_v:-0}"
            push _a
            ;;
        '?')
            eval_operation '@'
            eval_operation '.'
            ;;
        ''|'then'|';')
            ;;
        *[!0-9]*)
            eval "_worddef=\"\$word_$1\""
            if [ -z "$_worddef" ]
            then
                echo -n "[unknown word $1] "
            else
                push_repl_input
                eval_all "$_worddef"
                pop_repl_input
            fi
            ;;
        *)
            _num=$1
            clear "$DATA_MASK" _n
            while [ ${#_num} -ne 0 ]
            do
                _digit=$(get_first_char $_num)
                shleft "$DATA_MASK" _n_a _n
                shleft "$DATA_MASK" _n_b _n_a
                shleft "$DATA_MASK" _n_b _n_b
                _xc=0
                add "$DATA_MASK" _xc _n _n_a _n_b
                _xc=0
                add "$DATA_MASK" _xc _n _n dec_$_digit
                _num=${_num#?}
            done
            push _n
            ;;
    esac
}

handle_repl_string() {
    case "$repl_string_mode" in
        print)
            echo -n "$repl_string"
            ;;
        exec)
            eval "$repl_string"
            ;;
    esac
    repl_string_mode=0
}

# eval_all s
eval_all() {
    _repl_input="$1"
    while true
    do
        _aopr="${_repl_input%% *}"
        case "$repl_string_mode" in
            word)
                current_word="$_aopr"
                repl_string_mode=word_skip
                repl_string=''
                ;;
            word_skip)
                if [ "$_aopr" = ';' ]
                then
                    eval "word_$current_word=\$repl_string"
                    repl_string_mode=0
                else
                    repl_string="$repl_string$_aopr "
                fi
                ;;
            'if')
                if [ "$_aopr" = 'if' ]
                then
                    _xc=0
                    add "$STACKPTR_MASK" _xc if_level if_level const_1
                elif [ "$_aopr" = 'then' ]
                then
                    if eq "$STACKPTR_MASK" if_level const_0
                    then
                        repl_string_mode=0
                        pop _a
                        if eq "$DATA_MASK" _a const_0
                        then
                            :
                        else
                            push_repl_input
                            eval_all "$repl_string"
                            pop_repl_input
                        fi
                    fi
                    _xc=1
                    sub "$STACKPTR_MASK" _xc if_level if_level const_1
                fi
                repl_string="$repl_string$_aopr "
                ;;
            variable)
                eval "word_$_aopr='__v $_aopr '"
                repl_string_mode=0
                ;;
            variable_push)
                _push_string var_sp var_stack "$_aopr"
                repl_string_mode=0
                ;;
            0)
                eval_operation "$_aopr"
                ;;
            *)
                _aopr_strip="${_aopr%\"}"
                repl_string="$repl_string$_aopr_strip "
                [ "$_aopr_strip" != "$_aopr" ] && handle_repl_string
        esac
        _repl_input_old="$_repl_input"
        _repl_input="${_repl_input#* *}"
        [ "$_repl_input_old" = "$_repl_input" ] && break
    done
}

const_1_1=1
bitnot "$DATA_MASK" const_n1 const_0

repl_string_mode=0

while :
do
    read repl_input
    eval_all "$repl_input"
    echo ' ok'
done
